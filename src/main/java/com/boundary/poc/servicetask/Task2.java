package com.boundary.poc.servicetask;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class Task2 implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("-------------------------------> In Java Delegate task 2 for process: " + execution.getProcessInstanceId());
        System.out.println("-------------------------------> Flowable Type: " + execution.getVariable("flowableType"));
        System.out.println("-------------------------------> Flowable User ID: " + execution.getVariable("flowableUserId"));
//        execution.setVariable("flowableType", 2);
        System.out.println("-------------------------------> Post change:::: Flowable Type: " + execution.getVariable("flowableType"));

    }
}
