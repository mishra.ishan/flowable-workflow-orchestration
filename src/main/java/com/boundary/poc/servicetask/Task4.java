package com.boundary.poc.servicetask;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class Task4 implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("Exiting for type: " + execution.getVariable("flowableType") + " and userId: " + execution.getVariable("flowableUserId"));
    }
}
