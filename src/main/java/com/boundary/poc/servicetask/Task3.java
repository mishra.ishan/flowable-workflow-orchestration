package com.boundary.poc.servicetask;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Task3 implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("------------------> FINALLY In Java Delegate task 3 for process: " + execution.getProcessInstanceId());
        System.out.println(execution.getVariables());
    }
}
