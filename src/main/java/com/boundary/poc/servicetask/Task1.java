package com.boundary.poc.servicetask;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Task1 implements JavaDelegate{

    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("--------> In java delegate for task 1 for process: " + execution.getProcessInstanceId());
        execution.setVariable("flowableType", 2);
        System.out.println("--------> Post change from task 1: " + execution.getVariables());
    }
}
