package com.boundary.poc;

import com.boundary.poc.consumer.InputTopicConsumer;
import com.boundary.poc.servicetask.Task1;
import com.boundary.poc.servicetask.Task2;
import com.boundary.poc.servicetask.Task3;
import org.flowable.app.spring.SpringAppEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BoundaryEventPOC {
    public static void main(String[] args) {
        SpringApplication.run(BoundaryEventPOC.class, args);
    }

    @Bean
    public EngineConfigurationConfigurer<SpringAppEngineConfiguration> myBoundaryConfigurer(InputTopicConsumer inputTopicConsumer) {
        return engineConfiguration -> {
            engineConfiguration.addEventRegistryEventConsumer(inputTopicConsumer.getConsumerKey(), inputTopicConsumer);
        };
    }

    @Bean
    public InputTopicConsumer customInputEventConsumer() {
        return new InputTopicConsumer();
    }

    @Bean
    public Task1 createTask1() {
        return new Task1();
    }

    @Bean
    public Task2 createTask2() {
        return new Task2();
    }

    @Bean
    public Task3 createTask3() {
        return new Task3();
    }

}
