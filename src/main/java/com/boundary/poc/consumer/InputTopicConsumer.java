package com.boundary.poc.consumer;

import org.flowable.engine.RuntimeService;
import org.flowable.eventregistry.api.EventRegistryEvent;
import org.flowable.eventregistry.api.EventRegistryEventConsumer;
import org.springframework.beans.factory.annotation.Autowired;

public class InputTopicConsumer implements EventRegistryEventConsumer {

    @Override
    public void eventReceived(EventRegistryEvent event) {
        System.out.println("-------------------------> Event Received -> " + event.getType());
    }

    @Override
    public String getConsumerKey() {
        System.out.println("---------------------------> Consumer Key request .... Returning null");
        return null;
    }
}
