package com.pgsql.poc;

public enum Topic {
    INPUT("pit"),
    OUTPUT("pot");

    private String name;

    Topic(String name) {
        this.name = name;
    }
}
