//package com.pgsql.poc.eventregistry.configuration;
//
//import org.flowable.app.spring.SpringAppEngineConfiguration;
//import org.flowable.spring.SpringProcessEngineConfiguration;
//import org.flowable.spring.boot.EngineConfigurationConfigurer;
//import org.springframework.stereotype.Component;
//
//@Component
//public class CustomEventRegistryConfiguration implements EngineConfigurationConfigurer<SpringProcessEngineConfiguration> {
//
//    @Override
//    public void configure(SpringProcessEngineConfiguration engineConfiguration) {
//        engineConfiguration.setEventRegistryEventConsumer(new CustomInputEventConsumer());
//    }
//}
