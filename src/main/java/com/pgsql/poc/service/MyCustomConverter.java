package com.pgsql.poc.service;

import org.springframework.stereotype.Service;

@Service
public class MyCustomConverter {
    public Integer oddToEven(int number) {
        System.out.println("-----------------------> Before Conversion: " + number);
        return number % 2 == 0 ? number : number + 1;
    }

    public void printEverythingToConsole(int number, int number_old, String word) {
        System.out.println("---------------------> Number before transformation: "+number_old);
        System.out.println("---------------------> Word: "+word);
        System.out.println("---------------------> Number after transformation: "+number);
    }
}
