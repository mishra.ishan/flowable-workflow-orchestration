package com.pgsql.poc;

//import com.pgsql.poc.eventregistry.configuration.CustomEventRegistryConfiguration;
import com.pgsql.poc.eventregistry.configuration.CustomInputEventConsumer;
import com.pgsql.poc.service.MyCustomConverter;
import org.flowable.app.spring.SpringAppEngineConfiguration;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PGSqlPOC {
    public static void main(String[] args) {
        SpringApplication.run(PGSqlPOC.class, args);
    }

//    @Bean
//    public EngineConfigurationConfigurer<SpringProcessEngineConfiguration> eventRegistryConfigurator () {
//        return new CustomEventRegistryConfiguration();
//    }
    @Bean
    public EngineConfigurationConfigurer<SpringAppEngineConfiguration> myCustomAppEngineConfigurer(CustomInputEventConsumer customInputEventConsumer) {
        return engineConfiguration -> {
            engineConfiguration.addEventRegistryEventConsumer(customInputEventConsumer.getConsumerKey(), customInputEventConsumer);
        };
    }

    @Bean
    public CustomInputEventConsumer customInputEventConsumer() {
        return new CustomInputEventConsumer();
    }

    @Bean
    public MyCustomConverter createCustomConverter() {
        return new MyCustomConverter();
    }
}