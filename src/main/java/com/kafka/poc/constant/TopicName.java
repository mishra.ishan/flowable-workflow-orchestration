package com.kafka.poc.constant;

public enum TopicName {
    KAFKA_INPUT_TOPIC("input-test"),
    KAFKA_OUTPUT_TOPIC("output-test");

    private String topicName;

    TopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicName() {
        return topicName;
    }
}
