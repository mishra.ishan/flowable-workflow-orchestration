package com.kafka.poc.application;

import com.kafka.poc.service.CustomConversionService;
import com.kafka.poc.service.InputEventConsumer;
import org.flowable.app.spring.SpringAppEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KafkaPOCEventRegistry {
    public static void main(String[] args) {
        SpringApplication.run(KafkaPOCEventRegistry.class, args);
    }

    @Bean
    public InputEventConsumer inputEventConsumer() {
        return new InputEventConsumer();
    }

    @Bean
    public EngineConfigurationConfigurer<SpringAppEngineConfiguration> customAppEngineConfigurer(InputEventConsumer inputEventConsumer) {
        return engineConfiguration -> {
            engineConfiguration.addEventRegistryEventConsumer(inputEventConsumer.getConsumerKey(), inputEventConsumer);
        };
    }

    @Bean
    public CustomConversionService customConversionService() {
        return new CustomConversionService();
    }
}
