package com.kafka.poc.service;

import org.springframework.stereotype.Service;

@Service
public class CustomConversionService {

    public String upperCase (String lowercase) {
        System.out.println("------------------> Received 1:--" + lowercase);
        return lowercase.toUpperCase();
    }

    public void dummyPrint (String upperCase) {
        System.out.println(upperCase);
    }
}
