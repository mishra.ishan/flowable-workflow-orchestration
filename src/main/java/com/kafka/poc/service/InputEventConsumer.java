package com.kafka.poc.service;

import org.flowable.common.engine.api.FlowableIllegalArgumentException;
import org.flowable.eventregistry.api.EventRegistryEvent;
import org.flowable.eventregistry.api.EventRegistryEventConsumer;
import org.flowable.eventregistry.api.runtime.EventInstance;
import org.flowable.eventregistry.model.EventModel;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class InputEventConsumer implements EventRegistryEventConsumer {

    @Override
    public void eventReceived(EventRegistryEvent event) {
    }

    @Override
    public String getConsumerKey() {
        return "kafkaPOCEvent";
    }
}
